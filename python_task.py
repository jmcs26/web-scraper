from datetime import datetime, timedelta
import json
import logging
import requests
import sys
from bs4 import BeautifulSoup
import smtplib
import time
import pandas_datareader as pdr

LOG = logging.getLogger(__name__)

SLACK_WEBHOOK = "https://hooks.slack.com/services/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"

def post_message_to_slack(message_text):
    slack_response = requests.post(SLACK_WEBHOOK,
                                   json.dumps({'text': message_text}))
    LOG.debug("Response code from slack: " + str(slack_response.status_code))

    if slack_response.status_code != 200:
        LOG.error("Failed to post to slack: " + str(slack_response.status_code)
                  + " " + str(slack_response.reason))

    return slack_response

while True:
    
    url = "https://bitbucket.org/jmcs26/web-scraper/src/master/"
 
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    response = requests.get(url, headers=headers)
 
    soup = BeautifulSoup(response.text, "lxml")
    
   
    if str(soup).find("update") == -1:
        time.sleep(60)
        continue
        
  
    else:
           post_message_to_slack("<<<New Update on your Bitbucket Repository>>>")
    break       


